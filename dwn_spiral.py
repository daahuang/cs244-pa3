#!/usr/bin/python
"CS244 Spring 2015 Assignment 1: Bufferbloat"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

from monitor import monitor_qlen
import termcolor as T

import sys
import os
import math

# new import
import numpy as np
from playback_buffer import PlaybackBuffer
from custom_client import VidClient
import csv


# hard coded params
PB_BUFFER_SZ = 240 # 4 minutes as stated in the paper

CMP_FLOW_START = 50.0 #TODO: change, not used now
VID_RATES = np.array([235, 375, 560, 750, 1050, 1400, 1750])/1000.0 # in Mb/s, sorted

# Don't just read the TODO sections in this code.  Remember that
# one of the goals of this assignment is for you to learn how to use
# Mininet. :-)

parser = ArgumentParser(description="Bufferbloat tests")


parser.add_argument('--SEG_SZ', 
                    type=int,
                    help="RQ_SEG_SZ",
                    default=4)

parser.add_argument('--consrv', 
                    type=float,
                    help="consrv",
                    default=0.4)


parser.add_argument('--bw-host', '-B',
                    type=float,
                    help="Bandwidth of host links (Mb/s)",
                    default=100)


parser.add_argument('--rate_filter', 
		    type=str,
                    help="rate_filter",
                    default="mv_avg")

parser.add_argument('--bw-net', '-b',
                    type=float,
                    help="Bandwidth of bottleneck (network) link (Mb/s)",
                    required=True)

parser.add_argument('--delay',
                    type=float,
                    help="Link propagation delay (ms)",
                    required=True)

parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
                    required=True)

parser.add_argument('--time', '-t',
                    help="Duration (sec) to run the experiment",
                    type=int,
                    default=10)

parser.add_argument('--maxq',
                    type=int,
                    help="Max buffer size of network interface in packets",
                    default=100)

# Linux uses CUBIC-TCP by default that doesn't have the usual sawtooth
# behaviour.  For those who are curious, invoke this script with
# --cong cubic and see what happens...
# sysctl -a | grep cong should list some interesting parameters.
parser.add_argument('--cong',
                    help="Congestion control algorithm to use",
                    default="reno")

# Expt parameters
args = parser.parse_args()

################
# other helper
################
def save_tup_list_tofile(data, fname):
    with open(fname, 'w') as f:
        csv_out = csv.writer(f)
        for row in data:
            csv_out.writerow(row)

################
# from PA1
################

class DSTopo(Topo):
    "Simple topology for downward spiral effect experiment."
    """
    def build(self, n=2):
        # create hosts 
        srv = self.addHost('srv', cpu=.5/3)
        #host_vid = self.addHost('h_v', cpu=.5/3)
        host_cmp = self.addHost('h_c', cpu=.5/3)


        # Here I have created a switch.  If you change its name, its
        # interface names will change from s0-eth1 to newname-eth1.
        switch = self.addSwitch('s0')

        # Add links with appropriate characteristics
        #self.addLink(host_vid, switch, bw=args.bw_host, delay=args.delay, max_queue_size=args.maxq)
        self.addLink(host_cmp, switch, bw=args.bw_host, delay=args.delay, max_queue_size=args.maxq)
        self.addLink(srv, switch, bw=args.bw_net, delay=args.delay, max_queue_size=args.maxq)

        return
    """
    def build(self, n=2):
        # create three hosts
        host1 = self.addHost('h1', cpu=.5/3) # webserver
        host2 = self.addHost('h2', cpu=.5/3) # competing
        host3 = self.addHost('h3', cpu=.5/3) # streaming


        # Here I have created a switch.  If you change its name, its
        # interface names will change from s0-eth1 to newname-eth1.
        switch = self.addSwitch('s0')

        # Add links with appropriate characteristics
        self.addLink(host1, switch, bw=args.bw_net, delay=args.delay, max_queue_size=args.maxq)
        self.addLink(host2, switch, bw=args.bw_host, delay=args.delay, max_queue_size=args.maxq)
        self.addLink(host3, switch, bw=args.bw_host, delay=args.delay, max_queue_size=args.maxq)

        return

# Simple wrappers around monitoring utilities.  You are welcome to
# contribute neatly written (using classes) monitoring scripts for
# Mininet!
def start_tcpprobe(outfile="cwnd.txt"):
    os.system("rmmod tcp_probe; modprobe tcp_probe full=1;")
    Popen("cat /proc/net/tcpprobe > %s/%s" % (args.dir, outfile),
          shell=True)

def stop_tcpprobe():
    Popen("killall -9 cat", shell=True).wait()

def stop_iperf():
    Popen("killall -9 iperf", shell=True).wait()

def start_qmon(iface, interval_sec=0.1, outfile="q.txt"):
    monitor = Process(target=monitor_qlen,
                      args=(iface, interval_sec, outfile))
    monitor.start()
    return monitor
"""
def start_competing_iperfs(net):
    server = net.get('srv')
    print "Starting iperf server..."
    
    iperf_srv = server.popen("iperf -s -w 16m")
    
    h_vid = net.get('h_v')
    h_cmp = net.get('h_c')

    iperf_client_vid = h_vid.popen("iperf -c %s -t %d -i 1 > vid_iperf.txt" % (server.IP(), args.time), shell=True)
    iperf_client_cmp = h_cmp.popen("iperf -c %s -t %d -i 1 > cmp_iperf.txt" % (server.IP(), args.time), shell=True)

def start_competing_flow(net):
    h_cmp = net.get('h_c')
    print "Starting iperf server..."
    # For those who are curious about the -w 16m parameter, it ensures
    # that the TCP flow is not receiver window limited.  If it is,
    # there is a chance that the router buffer may not get filled up.
    server = h_cmp.popen("iperf -s -w 16m -i 1> test1.txt", shell=True)
    # TODO: Start the iperf client on h1.  Ensure that you create a
    # long lived TCP flow.
    srv = net.get('srv')
    client = srv.popen("iperf -c %s -t %d -i 1 > test.txt" % (h_cmp.IP(), args.time), shell=True ) #TODO: check how long to run, check how to use output
"""
    

def start_competing_flow(net):
    h2 = net.get('h2')
    print "Starting iperf server..."
    # For those who are curious about the -w 16m parameter, it ensures
    # that the TCP flow is not receiver window limited.  If it is,
    # there is a chance that the router buffer may not get filled up.
    server = h2.popen("iperf -s -w 16m", shell=True)
    # Start the iperf client on h1.  Ensure that you create a
    # long lived TCP flow.
    h1 = net.get('h1')
    client = h1.popen("iperf -c %s -t %d -i 5 > %s/cmp_flow_thruput.txt" % (h2.IP(), args.time-5, args.dir), shell=True ) # check how long to run, check how to use output

    # TODO: try curl with open-ended byte range?


def start_streaming_flow(net):
    h3 = net.get('h3')
    print "Starting iperf server..."
    server = h3.popen("iperf -s -w 16m", shell=True)
    # Start the iperf client on h1 (vid server).  Ensure that you create a
    # long lived TCP flow.
    h1 = net.get('h1')
    client = h1.popen("iperf -c %s -t %d -i 5 > stream_flow_thruput.txt" % (h3.IP(), args.time-5), shell=True )


def start_webserver(net):
    h1 = net.get('h1')
    proc = h1.popen("python http/webserver.py", shell=True)
    # proc = h1.popen("python -m RangeHTTPServer 80", shell=True)
    sleep(1)
    return [proc]
"""
def start_ping(net):
    # TODO: Start a ping train from h1 to h2 (or h2 to h1, does it
    # matter?)  Measure RTTs every 0.1 second.  Read the ping man page
    # to see how to do this.

    # Hint: Use host.popen(cmd, shell=True).  If you pass shell=True
    # to popen, you can redirect cmd's output using shell syntax.
    # i.e. ping ... > /path/to/ping.
    out_fname = os.path.join(args.dir, 'ping.txt')

    h1 = net.get('h1')
    h2 = net.get('h2')

    p = h1.popen("ping -i 0.1 %s > %s" % (h2.IP(), out_fname) , shell=True)

"""

def dwn_spiral():
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % args.cong)
    topo = DSTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    # This dumps the topology and how nodes are interconnected through
    # links.
    dumpNodeConnections(net.hosts)
    # This performs a basic all pairs ping test.
    net.pingAll()

    # Start all the monitoring processes
    start_tcpprobe("cwnd.txt")

    # Start monitoring the queue sizes.  Since the switch I
    # created is "s0", I monitor one of the interfaces.  Which
    # interface?  The interface numbering starts with 1 and increases.
    # Depending on the order you add links to your network, this
    # number may be 1 or 2.  Ensure you use the correct number.
    qmon = start_qmon(iface='s0-eth2', #TODO changed from eth2 to eth3?
                      outfile='%s/q.txt' % (args.dir))

    # Start iperf, webservers, etc.
    #start_competing_iperfs(net)
    #start_competing_flow(net) #TODO: turn off later
    start_webserver(net)
    #start_ping(net)


    # playback buffer
    PBBuffer = PlaybackBuffer(PB_BUFFER_SZ) #TODO: fix the PBBuffer threading
    PBBuffer.start()

    # rate selection algorithm
    vidClient = VidClient(VID_RATES, consrv =args.consrv, rate_filter =args.rate_filter, rate_filter_len = 10)

    ### Initilize our stuffs ###
    cmp_flow_started = False
    # initilize to lowest rate
    cur_pb_rate = vidClient.get_lowest_rate()

    # start_streaming_flow(net) # test 2 TCP flows

    #### init arrays
    buffer_all = []
    time_all = []
    pb_rate_all = []
    vid_thruput_all = []

    start_time = time()
    # web_f_times = [] #@
    h1 = net.get('h1')
    h3 = net.get('h3')
    while True:


        if PBBuffer.buffer_avail(args.SEG_SZ):
            # compute byte range based on rate
            num_bytes = args.SEG_SZ * cur_pb_rate * (10**6) / 8 # sec * rate * Mega / 8 => byte

            # use curl to requst the bytes
            fp = h3.popen("curl -r 0-%d -o /dev/null -s -w %%{time_total} %s:80/http/salad.mp4" % (num_bytes, h1.IP() ) )
            (out_time, err) = fp.communicate()
            print('vid_downalod_time: %f'% (float(out_time)))

            # put to PBBuffer
            assert PBBuffer.incre_buffer(args.SEG_SZ)
            print('buffer_size: %d' % PBBuffer.buffer)
            
            # estimate the speed
            est_thruput_bytes = num_bytes / float(out_time)
            est_thruput = (est_thruput_bytes * 8) / (10 ** 6)

            # update rate 
            cur_pb_rate = vidClient.get_pb_rate(est_thruput)
            print('current playback rate %f' % cur_pb_rate)
         
        else:

            # first time the buffer is full, start the competing flow
            if not cmp_flow_started:
                print("starting competing flow")
                start_competing_flow(net)
                cmp_flow_started = True

            # drain the PBBuffer
            print("draining buffer")
            sleep(0.5)



        now = time()
        delta = now - start_time
        
        """
        if delta > CMP_FLOW_START and not cmp_flow_started:
            print("starting competing flow")
            start_competing_flow(net)
            cmp_flow_started = True
        """

        #save stuffs to arrays
        time_all.append(now)
        pb_rate_all.append(cur_pb_rate)
        vid_thruput_all.append(est_thruput)
        buffer_all.append(PBBuffer.buffer)
            


        if delta > args.time:
            break
        print "%.1fs left..." % (args.time - delta)     


    # also stop PBBuffer
    PBBuffer.finish()

    # Hint: The command below invokes a CLI which you can use to
    # debug.  It allows you to run arbitrary commands inside your
    # emulated hosts h1 and h2.
    # CLI(net)

    stop_tcpprobe()
    stop_iperf()
    qmon.terminate()
    net.stop()
    # Ensure that all processes you create within Mininet are killed.
    # Sometimes they require manual killing.
    Popen("pgrep -f webserver.py | xargs kill -9", shell=True).wait()


    ###### save all the arrays ######
    save_tup_list_tofile(zip(time_all, buffer_all), os.path.join(args.dir, 'buffer.txt'))
    save_tup_list_tofile(zip(time_all, pb_rate_all), os.path.join(args.dir, 'pb_rate.txt'))
    save_tup_list_tofile(zip(time_all, vid_thruput_all), os.path.join(args.dir, 'vid_thruput.txt'))
    

if __name__ == "__main__":
    dwn_spiral()

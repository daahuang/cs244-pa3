import threading
import time


class PlaybackBuffer(threading.Thread):

    def __init__(self, buffer_size, interval=0.1):
        # init the thread
        threading.Thread.__init__(self)
        self.interval = interval  # seconds
        self.buffer_size = buffer_size
        self.buffer = 0.0

        self.alive = False


    def run(self):
        '''
        this will run in its own thread via self.start()
        '''
        self.alive = True
        while self.alive:
            time.sleep(self.interval)
            # update count value
            self.buffer -= self.interval
            self.check_buffer()

    def check_buffer(self):
        if self.buffer <= 0: 
            self.buffer = 0
        if self.buffer >= self.buffer_size:
            self.buffer = self.buffer_size

    def incre_buffer(self, inc):
        if self.buffer + inc <= self.buffer_size:
            self.buffer += inc
            self.check_buffer()
            return True
        else:
            return False


    def buffer_avail(self, inc):
        if self.buffer + inc <= self.buffer_size:
            return True
        else:
            return False

    def finish(self):
        '''
        close the thread, return final value
        '''
        # stop the while loop in method run
        self.alive = False
        return self.buffer

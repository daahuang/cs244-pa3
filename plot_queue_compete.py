'''
Plot queue occupancy over time
'''
from helper import *
import plot_defaults

from matplotlib.ticker import MaxNLocator
from pylab import figure

legends=["Video Rate","Video Throughput","Competing Flow"]
parser = argparse.ArgumentParser()


parser.add_argument('--title',
                    help="figure title",
                    type=str)

parser.add_argument('--files', '-f',
                    help="Queue timeseries output to one plot",
                    required=True,
                    action="store",
                    nargs='+',
                    dest="files")


parser.add_argument('--compete_file', '-compete',
                    help="buffer_file",
                    required=True,
		    type=str)

parser.add_argument('--legend', '-l',
                    help="Legend to use if there are multiple plots.  File names used as default.",
                    action="store",
                    nargs="+",
                    default=None,
                    dest="legend")

parser.add_argument('--out', '-o',
                    help="Output png file for the plot.",
                    default=None, # Will show the plot
                    dest="out")

parser.add_argument('--labels',
                    help="Labels for x-axis if summarising; defaults to file names",
                    required=False,
                    default=[],
                    nargs="+",
                    dest="labels")

parser.add_argument('--every',
                    help="If the plot has a lot of data points, plot one of every EVERY (x,y) point (default 1).",
                    default=1,
                    type=int)

args = parser.parse_args()

if args.legend is None:
    args.legend = []
    for file in args.files:
        args.legend.append(file)

to_plot=[]
def get_style(i):
    if i == 0:
        return {'color': 'red'}
    elif i==1:
        return {'color': 'black', 'ls': '-.'}
    elif i==2:
        return {'color': 'green', 'ls': '-'}

m.rc('figure', figsize=(24, 8))
fig = figure()
ax = fig.add_subplot(111)
for i, f in enumerate(args.files):
    data = read_list(f)
    xaxis = map(float, col(0, data))
    start_time = xaxis[0]
    xaxis = map(lambda x: x - start_time, xaxis)
    qlens = map(float, col(1, data))

    xaxis = xaxis[::args.every]
    qlens = qlens[::args.every]
    ax.plot(xaxis, qlens, label=legends[i], lw=2, **get_style(i))
    ax.xaxis.set_major_locator(MaxNLocator(4))



open_=open(args.compete_file)
lines=open_.readlines();
xaxis=[];
qlens=[];
tag=0
for line in lines:
    if tag==1:
	t1=line.find(" ")
	t2=line.find(" ",t1+1)
	t3=line.find("-",t2+1)
	xaxis.append(float(line[t2:t3]))
	t1=line.find("MBytes");
	t2=line.find("Mbits",t1+1);
	qlens.append(float(line[t1+6:t2]))
    if line.find("Interval")!=-1 and line.find("Bandwidth")!=-1:
	tag=1;

start_time=1000-xaxis[-1]
for i in range(len(xaxis)):
    xaxis[i]=xaxis[i]+start_time

ax.plot(xaxis, qlens, label=legends[2], lw=2, **get_style(2))

plt.legend()
plt.xlim([0,1000])
plt.title(args.title,fontsize=28)
if args.out:
    print 'saving to', args.out
    plt.savefig(args.out)
else:
    plt.show()

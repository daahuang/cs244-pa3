#!/bin/bash

# Note: Mininet must be run as root.  So invoke this shell script
# using sudo.

time=1000
#bwnet=1.5
bwnet=5
#  If you want the RTT to be 20ms what should the delay on each
# link be?  Set this value correctly.
delay=5 # RTT = 20ms

iperf_port=5001

case ${1} in                    
  "20")
	echo "Your choice is figure 20"
    consrv=0.4
    rate_filter=mv_avg
    seg_sz=4
	;;
  "21")
	echo "Your choice is figure 21"
    consrv=0.1
    rate_filter=mv_avg
    seg_sz=4
	;;
  "22")
	echo "Your choice is figure 22"
    consrv=0.1
    rate_filter=perc_80
    seg_sz=4
	;;
  "23")
	echo "Your choice is figure 23"
    consrv=0.1
    rate_filter=perc_80
    seg_sz=20
	;;
  "perc_80")
	echo "Your choice is just changing rate filter to percentile-80"
    consrv=0.4
    rate_filter=perc_80
    seg_sz=4
	;;
  "seg_20")
	echo "Your choice is just changing segment size to 20"
    consrv=0.4
    rate_filter=mv_avg
    seg_sz=20
	;;
  *)
	echo "Usage ${0} {20|21|22|23|perc_80|seg_20}"
	;;
esac

for qsize in 15; do # assume 1KB packet => 15KB = 120kbit
    #for consrv in 0.4 0.1; do
	#for rate_filter in mv_avg perc_80;do
	    dir=ds-q"$qsize"-consrv"$consrv"-rate_filter$rate_filter-seg$seg_sz
	    #  Run dwn_spiral.py here...
	    python dwn_spiral.py --bw-net $bwnet --delay $delay --dir $dir --time $time --maxq $qsize --SEG_SZ $seg_sz --consrv $consrv --rate_filter $rate_filter # mv_avg

	    #  Ensure the input file names match the ones you use in
	    # bufferbloat.py script.  Also ensure the plot file names match
	    # the required naming convention when submitting your tarball.
	    #python plot_tcpprobe.py -f $dir/cwnd.txt -o $dir/cwnd-iperf.png -p $iperf_port
	    #python plot_ping.py -f $dir/ping.txt -o $dir/rtt.png

	    #  upate this, now the style is different, and only plots rate and vid thruput
	    python plot_queue.py -f $dir/pb_rate.txt $dir/vid_thruput.txt -o $dir/vid.png -buffer "$dir"/buffer.txt --title qsize_"$qsize"_consrv_"$consrv"_rateFilter_"$rate_filter"
	    #python plot_queue_compete.py -f $dir/pb_rate.txt $dir/vid_thruput.txt --compete_file $dir/cmp_flow_thruput.txt -o $dir/compete.png --title qsize_"$qsize"_consrv_"$consrv"_rateFilter_"$rate_filter"_with_Competing_Flow
        ./clean.sh
        cp -p $dir/vid.png results/fig-"${1}".png
	#done
    #done
done

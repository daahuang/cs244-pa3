import collections
import numpy as np

class VidClient():
    def __init__(self, pb_rates, consrv = 0.4, rate_filter = 'mv_avg', rate_filter_len = 10):
        # consrv: conservatism, if observe 2.0Mb, highest rate is (1-consrv) * 2.0
        # rate_filter: which kind of filter to use
        # rate_filter_len: for how many samples do filtering
        self.consrv = consrv
        self.rate_filter = rate_filter
        # self.rate_filter_len = rate_filter_len

        self.deque = collections.deque([], rate_filter_len)

        self.pb_rates = np.sort(pb_rates)

    def get_pb_rate(self, cur_thruput):
        # put sample to deque
        self.deque.append(cur_thruput)
        
        # estimate throughput with filter
        if self.rate_filter == 'mv_avg':
            est_thruput = sum(self.deque) / float(len(self.deque))
        elif self.rate_filter == 'perc_80':
            sorted_deque = np.sort(self.deque)
            deque_len = len(sorted_deque)
            idx = np.ceil(0.8 * deque_len) - 1
            idx = int(idx)
            est_thruput = sorted_deque[idx]
   
        pb_rate = self._get_pb_rate(est_thruput)
        return pb_rate

    def _get_pb_rate(self, est_thruput):
        # apply conservatism
        consrv_est_thruput = est_thruput * (1 - self.consrv)

        idx = np.where(consrv_est_thruput >= self.pb_rates)[0]
        if len(idx) > 0:
            idx = idx[-1] # find the largest one that is smaller than the thruput
        else:
            idx = 0 # smaller than all the rates, then just use the smallest rate
        
        return self.pb_rates[idx]

    def get_lowest_rate(self):
        return self.pb_rates[0]



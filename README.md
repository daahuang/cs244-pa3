# README #

1. Setup environment: run "sudo ./setup.sh" for Mininet/python setup. If you already have Mininet/python setup from PA1, then just run "sudo pip install rangehttpserver".
2. Generate all figures (Figure 20~23): run "sudo ./run_all.sh". The results will be copied to results/fig-2x.png. Our results are results/fig-2x-ori.png
3. Generate a specific figure: run "sudo ./run.sh [FIG]" FIG=[20|21|22|23|perc_80|seg_20]

* In case of errors, run "./clean.sh" to reset Mininet and kill processes

git clone git://github.com/mininet/mininet
mininet/util/install.sh -a

sudo apt-get install build-essential 
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
cd /usr
sudo chmod 777 src
cd /usr/src
sudo apt-get install python-pip

export LC_ALL=C
sudo apt-get install python-dev
sudo pip install numpy
sudo pip install matplotlib
sudo pip install argparse
sudo pip install termcolor
sudo pip install rangehttpserver

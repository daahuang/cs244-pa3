#!/bin/bash

# Note: Mininet must be run as root.  So invoke this shell script
# using sudo.

time=1000
#bwnet=1.5
bwnet=5
# TODO: If you want the RTT to be 20ms what should the delay on each
# link be?  Set this value correctly.
delay=5 # RTT = 20ms

iperf_port=5001

for qsize in 15; do # assume 1KB packet => 15KB = 120kbit
    for consrv in 0.4; do
	dir=ds-q"$qsize"-consrv"$consrv"
	echo $dir
	python plot_queue.py -f $dir/pb_rate.txt $dir/vid_thruput.txt -o $dir/vid.png -buffer "$dir"/buffer.txt
    done
done
